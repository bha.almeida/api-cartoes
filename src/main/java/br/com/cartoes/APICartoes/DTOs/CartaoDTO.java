package br.com.cartoes.APICartoes.DTOs;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class CartaoDTO {

    @NotNull(message = "Número do cartão inválido")
    private int numero;

    @NotNull(message = "Id do cliente inválido")
    @Min(value = 1, message = "Id do cliente deve ser maior que 1")
    private int clienteId;

    public CartaoDTO() {
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
