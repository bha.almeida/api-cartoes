package br.com.cartoes.APICartoes.DTOs;

public class AtivarCartaoDTO {

    private boolean ativo;

    public boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
