package br.com.cartoes.APICartoes.DTOs;

public class ConsultaCartaoDTO {

    private int id;
    private int numero;
    private int clienteId;

    public ConsultaCartaoDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
