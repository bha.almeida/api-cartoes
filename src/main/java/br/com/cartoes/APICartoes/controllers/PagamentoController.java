package br.com.cartoes.APICartoes.controllers;


import br.com.cartoes.APICartoes.DTOs.CadastrarPagamentoDTO;
import br.com.cartoes.APICartoes.DTOs.PagamentoCadastradoDTO;
import br.com.cartoes.APICartoes.models.Pagamento;
import br.com.cartoes.APICartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoCadastradoDTO criarPagamento(@RequestBody @Valid CadastrarPagamentoDTO cadastrarPagamentoDTO){
        try {
           return pagamentoService.criarPagamento(cadastrarPagamentoDTO);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id_cartao}")
    public List<PagamentoCadastradoDTO> consultarPagamentosCartao(@PathVariable(name = "id_cartao") int idCartao){
        try {
            return pagamentoService.consultarPagamentosCartao(idCartao);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
