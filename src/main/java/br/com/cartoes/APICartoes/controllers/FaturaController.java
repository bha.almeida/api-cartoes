package br.com.cartoes.APICartoes.controllers;

import br.com.cartoes.APICartoes.DTOs.PagamentoCadastradoDTO;
import br.com.cartoes.APICartoes.models.Fatura;
import br.com.cartoes.APICartoes.services.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    private FaturaService faturaService;

    @GetMapping("/{cliente-id}/{cartao-id}")
    public List<PagamentoCadastradoDTO> exibirFatura(@PathVariable(name = "cliente-id") int clienteId,
                                                     @PathVariable(name = "cartao-id") int cartaoId){
        try {
            return faturaService.exibirFatura(clienteId, cartaoId);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

//    @PostMapping("{cliente-id}/{cartao-id}/pagar")
//    public Fatura pagarFatura(@PathVariable(name = "cliente-id") int clienteId,
//                              @PathVariable(name = "cartao-id") int cartaoId){
//        try {
//            return faturaService.pagarFatura(clienteId, cartaoId);
//        } catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
//        }
//
//    }

}
