package br.com.cartoes.APICartoes.controllers;

import br.com.cartoes.APICartoes.DTOs.AtivarCartaoDTO;
import br.com.cartoes.APICartoes.DTOs.CartaoCriadoDTO;
import br.com.cartoes.APICartoes.DTOs.CartaoDTO;
import br.com.cartoes.APICartoes.DTOs.ConsultaCartaoDTO;
import br.com.cartoes.APICartoes.models.Cartao;
import br.com.cartoes.APICartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoCriadoDTO cadastrarCartao(@RequestBody @Valid CartaoDTO cartaoDTO){
        try {
            return cartaoService.criarCartao(cartaoDTO);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PatchMapping("/{numero}")
    public CartaoCriadoDTO alteraStatusCartao(@PathVariable(name = "numero") int numero,
                                     @RequestBody AtivarCartaoDTO cartao){
        try {
            return cartaoService.alteraStatusCartao(numero, cartao);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public ConsultaCartaoDTO consultarCartao(@PathVariable(name = "numero") int numero){
        try {
            return cartaoService.consultarCartao(numero);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
