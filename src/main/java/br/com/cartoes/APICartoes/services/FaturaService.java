package br.com.cartoes.APICartoes.services;

import br.com.cartoes.APICartoes.DTOs.PagamentoCadastradoDTO;
import br.com.cartoes.APICartoes.models.Cartao;
import br.com.cartoes.APICartoes.models.Cliente;
import br.com.cartoes.APICartoes.models.Fatura;
import br.com.cartoes.APICartoes.models.Pagamento;
import br.com.cartoes.APICartoes.repositories.FaturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FaturaService {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private FaturaRepository faturaRepository;

    public List<PagamentoCadastradoDTO> exibirFatura(int clienteId, int cartaoId){

        return pagamentoService.consultarPagamentosCartao(cartaoId);
    }

//    public Fatura pagarFatura(int clienteId, int cartaoId){
//        pagamentoService.deletarPagamentosPorCartao(cartaoId);
//        return faturaRepository.save(fatura);
//    }
}
