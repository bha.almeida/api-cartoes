package br.com.cartoes.APICartoes.services;

import br.com.cartoes.APICartoes.DTOs.CadastrarPagamentoDTO;
import br.com.cartoes.APICartoes.DTOs.PagamentoCadastradoDTO;
import br.com.cartoes.APICartoes.models.Cartao;
import br.com.cartoes.APICartoes.models.Pagamento;
import br.com.cartoes.APICartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private PagamentoRepository pagamentoRepository;

    public PagamentoCadastradoDTO criarPagamento(CadastrarPagamentoDTO cadastrarPagamentoDTO){
        Cartao cartaoDB = cartaoService.buscarCartaoPorId(cadastrarPagamentoDTO.getCartao_id());

        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(cadastrarPagamentoDTO.getDescricao());
        pagamento.setValor(cadastrarPagamentoDTO.getValor());
        pagamento.setCartao(cartaoDB);
        pagamentoRepository.save(pagamento);

        PagamentoCadastradoDTO pagamentoCadastradoDTO = new PagamentoCadastradoDTO();
        pagamentoCadastradoDTO.setId(pagamento.getId());
        pagamentoCadastradoDTO.setCartao_id(pagamento.getCartao().getId());
        pagamentoCadastradoDTO.setDescricao(pagamento.getDescricao());
        pagamentoCadastradoDTO.setValor(pagamento.getValor());

        return pagamentoCadastradoDTO;
    }

    public List<PagamentoCadastradoDTO> consultarPagamentosCartao(int idCartao){

        Cartao cartaoDB = cartaoService.buscarCartaoPorId(idCartao);
        List<PagamentoCadastradoDTO> listaPagamentos = new ArrayList<>();
        for(Pagamento pagamento : cartaoDB.getPagamentos()){
            PagamentoCadastradoDTO pagamentoCadastradoDTO = new PagamentoCadastradoDTO();
            pagamentoCadastradoDTO.setId(pagamento.getId());
            pagamentoCadastradoDTO.setCartao_id(cartaoDB.getId());
            pagamentoCadastradoDTO.setDescricao(pagamento.getDescricao());
            pagamentoCadastradoDTO.setValor(pagamento.getValor());

            listaPagamentos.add(pagamentoCadastradoDTO);
        }
        return listaPagamentos;
    }

    public void deletarPagamentosPorCartao(int idCartao){
        Cartao cartaoDB = cartaoService.buscarCartaoPorId(idCartao);
        pagamentoRepository.deleteByCartao(cartaoDB);
    }
}
