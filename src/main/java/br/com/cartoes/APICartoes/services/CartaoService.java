package br.com.cartoes.APICartoes.services;

import br.com.cartoes.APICartoes.DTOs.AtivarCartaoDTO;
import br.com.cartoes.APICartoes.DTOs.CartaoCriadoDTO;
import br.com.cartoes.APICartoes.DTOs.CartaoDTO;
import br.com.cartoes.APICartoes.DTOs.ConsultaCartaoDTO;
import br.com.cartoes.APICartoes.models.Cartao;
import br.com.cartoes.APICartoes.models.Cliente;
import br.com.cartoes.APICartoes.models.Pagamento;
import br.com.cartoes.APICartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;

    public CartaoCriadoDTO criarCartao(CartaoDTO cartaoDTO){
        Cartao cartao = new Cartao();
        Cliente cliente = clienteService.buscarCliente(cartaoDTO.getClienteId());

        cartao.setNumero(cartaoDTO.getNumero());
        cartao.setCliente(cliente);

        cartaoRepository.save(cartao);

        CartaoCriadoDTO cartaoCriadoDTO = new CartaoCriadoDTO();
        cartaoCriadoDTO.setId(cartao.getId());
        cartaoCriadoDTO.setNumero(cartao.getNumero());
        cartaoCriadoDTO.setAtivo(cartao.getAtivo());
        cartaoCriadoDTO.setClienteId(cartao.getCliente().getId());

        return cartaoCriadoDTO;
    }

    private Cartao buscarCartao(int numeroCartao){
        Optional<Cartao> optionalCartao = cartaoRepository.findByNumero(numeroCartao);

        if(optionalCartao.isPresent()){
            return optionalCartao.get();
        } else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public CartaoCriadoDTO alteraStatusCartao(int numeroCartao, AtivarCartaoDTO cartao){
        Cartao cartaoDB = buscarCartao(numeroCartao);
        cartaoDB.setAtivo(cartao.getAtivo());


        Cartao cartaoAlterado = cartaoRepository.save(cartaoDB);
        CartaoCriadoDTO cartaoCriadoDTO = new CartaoCriadoDTO();
        cartaoCriadoDTO.setId(cartaoAlterado.getId());
        cartaoCriadoDTO.setNumero(cartaoAlterado.getNumero());
        cartaoCriadoDTO.setAtivo(cartaoAlterado.getAtivo());
        cartaoCriadoDTO.setClienteId(cartaoAlterado.getId());

        return cartaoCriadoDTO;
    }

    public ConsultaCartaoDTO consultarCartao(int numeroCartao){
        Cartao cartao = this.buscarCartao(numeroCartao);
        ConsultaCartaoDTO consultaCartaoDTO = new ConsultaCartaoDTO();
        consultaCartaoDTO.setId(cartao.getId());
        consultaCartaoDTO.setNumero(cartao.getNumero());
        consultaCartaoDTO.setClienteId(cartao.getId());

        return consultaCartaoDTO;
    }

    public Cartao buscarCartaoPorId(int id){
        Optional<Cartao> optionalCartao = cartaoRepository.findById(id);

        if(optionalCartao.isPresent()){
            return optionalCartao.get();
        } else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }
}
