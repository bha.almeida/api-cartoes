package br.com.cartoes.APICartoes.repositories;

import br.com.cartoes.APICartoes.models.Cartao;
import br.com.cartoes.APICartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    public void deleteByCartao(Cartao cartao);
}
