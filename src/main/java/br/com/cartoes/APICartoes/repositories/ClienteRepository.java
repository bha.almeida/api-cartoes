package br.com.cartoes.APICartoes.repositories;

import br.com.cartoes.APICartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
