package br.com.cartoes.APICartoes.repositories;

import br.com.cartoes.APICartoes.models.Fatura;
import org.springframework.data.repository.CrudRepository;

public interface FaturaRepository extends CrudRepository<Fatura, Integer> {
}
